#include "ledconif.h"
#include "ledconif.c"

#include "cgreen/cgreen.h"
#include "cgreen/mocks.h"


static void setup(void)
{
    cgreen_mocks_are(loose_mocks);
    LEDCONIF_init();
    cgreen_mocks_are(strict_mocks);
}


static void teardown(void)
{

}


/*
State machine should remain in IDLE if no serial data received
*/
Ensure(testcase_remain_idle)
{
    statemachine.next_state = IDLE;
    expect(WDT_reset_timer);
    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(IDLE));

    expect(WDT_reset_timer);
    LEDCONIF_task();
    assert_that(statemachine.state, is_equal_to(IDLE));
}


/*
State machine should transition from IDLE to RED
*/
Ensure(testcase_transition_idle_red)
{
    serial_data.data = START_CHAR;
    serial_data.received = true;

    expect(WDT_reset_timer);
    LEDCONIF_task();

    assert_that(statemachine.next_state, is_equal_to(RED));

    LEDCONIF_task();
    assert_that(statemachine.state, is_equal_to(RED));
}


/*
State machine should transition from RED to GREEN
*/
Ensure(testcase_transition_red_green)
{
    serial_data.data = 0U;
    serial_data.received = true;
    statemachine.next_state = RED;

    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(GREEN));

    LEDCONIF_task();
    assert_that(statemachine.state, is_equal_to(GREEN));
}


/*
Serial data should be consume on state transition
*/
Ensure(testcase_consume_serial_data_transition)
{
    serial_data.data = 0U;
    serial_data.received = true;
    statemachine.next_state = RED;

    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(GREEN));

    LEDCONIF_task();
    assert_that(statemachine.state, is_equal_to(GREEN));
    assert_that(serial_data.received, is_equal_to(false));
}


/*
State machine should not transition when no serial data
*/
Ensure(testcase_no_transition_when_no_serial_data)
{
    serial_data.received = false;
    statemachine.next_state = RED;

    LEDCONIF_task();
    assert_that(statemachine.state, is_equal_to(RED));
    assert_that(statemachine.next_state, is_equal_to(RED));
}


/*
State machine should not transition from DONE if LED controller IPC is not available
*/
Ensure(testcase_no_transition_from_done_when_ipc_unavailable)
{
    statemachine.next_state = DONE;
    expect(LEDCON_IPC_mutex_take, will_return(false));

    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(DONE));
}


/*
State machine should transition from DONE if LED controller IPC is available
*/
Ensure(testcase_transition_from_done_when_ipc_available)
{
    statemachine.next_state = DONE;
    expect(LEDCON_IPC_mutex_take, will_return(true));
    expect(LEDCON_IPC_write_led_array);
    expect(LEDCON_IPC_set_ready_to_read);
    expect(LEDCON_IPC_mutex_give);

    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(ACK));
}


/*
State machine ACK should transmit acknowledge over serial then transition to IDLE
*/
Ensure(testcase_ack)
{
    statemachine.next_state = ACK;
    expect(USART0_tx, when(data, is_equal_to(ACK_CHAR)));

    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(IDLE));
}


/*
State machine should echo ACK when it receives an ACK from peer
*/
Ensure(testcase_echo)
{
    statemachine.state = IDLE;
    expect(WDT_reset_timer);
    LEDCONIF_task();

    serial_data.data = PING_CHAR;
    serial_data.received = true;
    expect(WDT_reset_timer);
    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(ACK));

    expect(USART0_tx, when(data, is_equal_to(ACK_CHAR)));
    LEDCONIF_task();
    assert_that(statemachine.next_state, is_equal_to(IDLE));
}


int main(void)
{
    TestSuite *testsuite = create_test_suite();
    set_setup(testsuite, setup);
    set_teardown(testsuite, teardown);

    add_test(testsuite, testcase_remain_idle);
    add_test(testsuite, testcase_transition_idle_red);
    add_test(testsuite, testcase_transition_red_green);
    add_test(testsuite, testcase_consume_serial_data_transition);
    add_test(testsuite, testcase_no_transition_when_no_serial_data);
    add_test(testsuite, testcase_no_transition_from_done_when_ipc_unavailable);
    add_test(testsuite, testcase_transition_from_done_when_ipc_available);
    add_test(testsuite, testcase_ack);
    add_test(testsuite, testcase_echo);

    return run_test_suite(testsuite, create_text_reporter());
}
