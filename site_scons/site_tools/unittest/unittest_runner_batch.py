"""
Unittest Runner Batch - Manages unit test batch processes
"""

from argparse import ArgumentParser
import os
import sys

from prettytable import PrettyTable
from unittest_runner import UnittestRunner, Unittest

__author__ = "jtran"
__version__ = "1.1.0"


class UnittestRunnerBatch(object):
    RESULT_HEADER = ["Index", "Test", "Result"]
    RESULT_PASSED = "Passed"
    RESULT_FAILED = "Failed"

    def __init__(self, unittests, fail_immediately=False, **kwargs):
        self._unittests = unittests
        self._fail_immediately = fail_immediately

        self._unittest_runners = []
        for unittest in unittests:
            new_unittest_runner = UnittestRunner(unittest=unittest, **kwargs)
            self._unittest_runners.append(new_unittest_runner)

        self._result_table = PrettyTable(self.RESULT_HEADER)

    """
    Public methods
    """
    def run(self):
        error = 0
        for err in self.run_iter():
            error |= err
        return error

    def run_iter(self):
        error = 0
        self._reset_result_table()
        for index, unittest_runner in enumerate(self._unittest_runners, start=1):
            error |= unittest_runner.run()

            self._add_unittest_runner_result(index=index, unittest_runner=unittest_runner)

            yield error

            if self._fail_immediately and error:
                break

    """
    Private methods
    """
    def _reset_result_table(self):
        self._result_table = PrettyTable(self.RESULT_HEADER)


    def _add_unittest_runner_result(self, index, unittest_runner):
        assert isinstance(unittest_runner, UnittestRunner)
        row = [
            index,
            os.path.splitext(os.path.basename(unittest_runner.exe_filepath))[0],
            self.RESULT_PASSED if (not unittest_runner.error) else self.RESULT_FAILED
        ]
        self._result_table.add_row(row)


    """
    Accessors
    """
    @property
    def result_table(self):
        return self._result_table

    """
    Accessors - advance
    """
    @property
    def unittest_runners(self):
        return self._unittest_runners



def get_args():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "-i", "--input",
        type=str,
        required=True,
        action="append"
    )
    arg_parser.add_argument(
        "-o", "--output",
        type=str,
        default=None,
        action="append"
    )
    arg_parser.add_argument(
        "--verbose",
        action="store_true"
    )
    return arg_parser.parse_args()


def main():
    args = get_args()
    exe_filepaths = args.input
    output_filepaths = args.output

    assert len(exe_filepaths) == len(output_filepaths)

    unittests = []
    for exe_filepath, output_filepath in zip(exe_filepaths, output_filepaths):
        new_unittest = Unittest(exe_filepath, output_filepath)
        unittests.append(new_unittest)

    unittest_runner_batch = UnittestRunnerBatch(unittests, fail_immediately=False, verbose=args.verbose)
    error = unittest_runner_batch.run()
    
    print("Operation summary:\n{}".format(unittest_runner_batch.result_table))

    return error


if __name__ == "__main__":
    sys.exit(main())
