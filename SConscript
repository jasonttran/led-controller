import os

from fsops import scan_tree, filter_files, ch_filename_ext

__author__ = "jtran"
__version__ = "1.1.0"


"""
Define naming conventions
"""
TARGET_NAME = Dir(".").name


"""
CLI arguments
"""
ENABLE_UNITTEST = GetOption("unittest")
ENABLE_VERBOSE = GetOption("verbose")


"""
Define file nodes and directory nodes
"""
PROJECT_DIR = Dir(".")
VARIANT_DIR = Dir("build")
OBJ_DIR = VARIANT_DIR.Dir("obj")

UNITTEST_DIR = PROJECT_DIR.Dir("test")
UNITTEST_SOURCE_DIR = UNITTEST_DIR.Dir("source")
UNITTEST_BUILD_DIR = VARIANT_DIR.Dir("test")
UNITTEST_MOCK_BUILD_DIR = UNITTEST_BUILD_DIR.Dir("mock")


INCLUDE_DIRS = [
    Dir("source"),
]

INCLUDE_DIRS_ROOT = [
    Dir("source/os"),
    Dir("source/light_ws2812"),
]

SRC_DIRS = [
    Dir("source"),
]

LINKER_FILES = []

EXCLUDED_SRC_FILES = []

UNITTEST_MOCK_DIRS = [
    Dir("source")
]


"""
Define build environments
"""
Import("avr_env")
avr_env.VariantDir(variant_dir=VARIANT_DIR.name, src_dir=SRC_DIRS, duplicate=0)


"""
Search and group build files
"""

""" Search and group source files and source directories """
target_src_filenodes = []
target_src_dirnodes = []

for dirnode in SRC_DIRS:
    src_filenodes, _, src_dirnodes, _ = scan_tree(dirnode)
    target_src_filenodes.extend(src_filenodes)
    target_src_dirnodes.extend(src_dirnodes)


""" Search and group linker scripts """
for linker_file in LINKER_FILES:
    avr_env["LINKFLAGS"].append("-T{}".format(File(linker_file).abspath))

""" Search and group include paths """
avr_env["CPPPATH"].extend(INCLUDE_DIRS)

for dirnode in INCLUDE_DIRS_ROOT:
    _, _, _, include_dirnodes = scan_tree(dirnode)
    avr_env["CPPPATH"].extend(include_dirnodes)

""" Filter build files """
target_src_filenodes = filter_files(target_src_filenodes, EXCLUDED_SRC_FILES)


"""
Perform builds
"""
obj_filenodes = []
for src_filenode in target_src_filenodes:
    new_filename = ch_filename_ext(src_filenode, "o")
    dest_filepath = OBJ_DIR.File(new_filename.name)
    new_obj_filenodes = avr_env.Object(target=dest_filepath, source=src_filenode)
    obj_filenodes.extend(new_obj_filenodes)

elf_filenodes = avr_env.Program(target=VARIANT_DIR.File("{}.elf".format(TARGET_NAME)), source=obj_filenodes)
bin_filenodes = avr_env.Objcopy(target=VARIANT_DIR.File("{}.bin".format(TARGET_NAME)), source=elf_filenodes)
hex_filenodes = avr_env.Objcopy(target=VARIANT_DIR.File("{}.hex".format(TARGET_NAME)), source=elf_filenodes)
lst_filenodes = avr_env.Objdump(target=VARIANT_DIR.File("{}.lst".format(TARGET_NAME)), source=elf_filenodes)
avr_env.Size(elf_filenodes)

Depends(elf_filenodes, LINKER_FILES)


"""
Unit test
"""
if ENABLE_UNITTEST:
    Import("unittest_env")
    unittest_env["CPPPATH"] += avr_env["CPPPATH"]
    unittest_env["CPPDEFINES"] += avr_env["CPPDEFINES"]

    all_mock_headers = []
    for mock_dir in UNITTEST_MOCK_DIRS:
        _, mock_headers, _, _ = scan_tree(mock_dir, recursive=False)
        all_mock_headers.extend(mock_headers)

    ut_sources, _, _, _ = scan_tree(UNITTEST_SOURCE_DIR, recursive=False)

    mock_targets = unittest_env.Mock(
        target_dir=UNITTEST_MOCK_BUILD_DIR,
        sources=all_mock_headers,
    )

    ut_exes, ut_objs, ut_mock_objs = unittest_env.Build(
        target_dir=UNITTEST_BUILD_DIR,
        sources=ut_sources,
        mocks=mock_targets,
    )

    results = unittest_env.RunBatch(ut_exes, verbose=ENABLE_VERBOSE)
