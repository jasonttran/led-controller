#include "clock.h"
#include "system.h"

#include <stdbool.h>
#include <stdint.h>
#include <util/delay.h>

#include "heartbeat.h"
#include "ledconif.h"
#include "ledcon.h"
#include "led.h"

#include "light_ws2812.h"


static void task_1hz(void);
static void task_100hz(void);
static void task_1khz(void);


void SYSTEM_init(void)
{
    HEARTBEAT_init();
    LEDCON_init();
    LEDCONIF_init();
}


void SYSTEM_run(void)
{
    static uint32_t counter_ms = 0U;

    while (true) {
        task_1khz();

        if (counter_ms % 10 == 0) {
            task_100hz();
        }

        if (counter_ms % 1000 == 0) {
            counter_ms = 0;
            task_1hz();
        }

        counter_ms++;
        _delay_ms(1);
    }
}


static void task_1hz(void)
{
    HEARTBEAT_task();
}


static void task_100hz(void)
{

}


static void task_1khz(void)
{
    LEDCONIF_task();
    LEDCON_task();
}
