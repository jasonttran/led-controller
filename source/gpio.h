#ifndef GPIO_H
#define GPIO_H

#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>

typedef volatile uint8_t SFR_t;

/* GPIO special function registers */
#define DATA_DIR_REG_B  (&DDRB)
#define DATA_DIR_REG_C  (&DDRC)
#define DATA_DIR_REG_D  (&DDRD)
#define DATA_REG_B      (&PORTB)
#define DATA_REG_C      (&PORTC)
#define DATA_REG_D      (&PORTD)
#define INPUT_REG_B     (&PINB)
#define INPUT_REG_C     (&PINC)
#define INPUT_REG_D     (&PIND)

/*
 * For pin offsets: use the macro PINXn
 * where 'X' is the port and 'n' is the pin offset
 *
 * Example: PINB5; PINC0; PIND2;
 */

void GPIO_config_mode(SFR_t* data_dir_reg, uint8_t pin, bool output);
void GPIO_config_output(SFR_t* data_dir_reg, uint8_t pin);
void GPIO_config_input(SFR_t* data_dir_reg, uint8_t pin);

void GPIO_set(SFR_t* data_reg, uint8_t pin);
void GPIO_clear(SFR_t* data_reg, uint8_t pin);
void GPIO_toggle(SFR_t* data_reg, uint8_t pin);

bool GPIO_get(SFR_t* input_reg, uint8_t pin);


#endif  // GPIO_H
