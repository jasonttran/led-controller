#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

#define SYSTEM_CONFIG_USART_BAUD 57600U
#define SYSTEM_CONFIG_LED_ARRAY_SIZE 3U

#endif  // SYSTEM_CONFIG_H
