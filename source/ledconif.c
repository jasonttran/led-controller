#include "ledconif.h"
#include "clock.h"

#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "ledcon_ipc.h"
#include "system_config.h"
#include "usart.h"
#include "wdt.h"

#define USART_BAUD SYSTEM_CONFIG_USART_BAUD

#define START_CHAR '^'
#define ACK_CHAR '$'
#define REQUEST_CHAR '?'
#define PING_CHAR '!'

typedef struct {
    uint8_t data;
    bool received;
} serialData_S;

typedef enum {
    IDLE=0,
    RED,
    GREEN,
    BLUE,
    DONE,
    ACK,
} state_E;

typedef struct {
    state_E state;
    state_E next_state;
    bool ledcon_processed;
    RGB_S led_buffer;
} stateMachine_S;

static volatile serialData_S serial_data;
static stateMachine_S statemachine;

static inline void next_state_logic(void);
static inline void output_logic(void);

/* State machine helper functions */
static inline bool process_ledcon(void);
static inline bool get_serial_data(uint8_t *data);


void LEDCONIF_init(void)
{
    serial_data.data = 0U;
    serial_data.received = false;

    statemachine.state = IDLE;
    statemachine.next_state = IDLE;
    statemachine.ledcon_processed = false;
    memset(&statemachine.led_buffer, 0U, sizeof(RGB_S));

    USART0_init((uint32_t)USART_BAUD);
    USART0_enable_rx_interrupt();

    WDT_init();
}


void LEDCONIF_task(void)
{
    statemachine.state = statemachine.next_state;
    output_logic();
    next_state_logic();
}


static inline void output_logic(void)
{
    switch (statemachine.state) {
        case IDLE:
        {
            WDT_reset_timer();
            break;
        }
        case RED:
        {
            break;
        }
        case GREEN:
        {
            break;
        }
        case BLUE:
        {
            break;
        }
        case DONE:
        {
            statemachine.ledcon_processed = process_ledcon();
            break;
        }
        case ACK:
        {
            USART0_tx((uint8_t)ACK_CHAR);
            USART0_clear_rx();
            break;
        }
        default:
            break;
    }
}


static inline void next_state_logic(void)
{
    uint8_t rx_data = 0U;
    bool received = get_serial_data(&rx_data);

    switch (statemachine.state) {
        case IDLE:
        {
            statemachine.ledcon_processed = false;

            if (received && ((char)rx_data == START_CHAR)) {
                statemachine.next_state = RED;
                USART0_tx((uint8_t)REQUEST_CHAR);
            }
            else if (received && ((char)rx_data == PING_CHAR)) {  // Echo
                statemachine.next_state = ACK;
            }
            else {
                // Pass
            }
            break;
        }
        case RED:
        {
            if (received) {
                statemachine.led_buffer.red = rx_data;
                statemachine.next_state = GREEN;
                USART0_tx((uint8_t)REQUEST_CHAR);
            }
            break;
        }
        case GREEN:
        {
            if (received) {
                statemachine.led_buffer.green = rx_data;
                statemachine.next_state = BLUE;
                USART0_tx((uint8_t)REQUEST_CHAR);
            }
            break;
        }
        case BLUE:
        {
            if (received) {
                statemachine.led_buffer.blue = rx_data;
                statemachine.next_state = DONE;
            }
            break;
        }
        case DONE:
        {
            if (statemachine.ledcon_processed) {
                statemachine.next_state = ACK;
            }
            break;
        }
        case ACK:
        {
            statemachine.next_state = IDLE;
            break;
        }
        default:
            break;
    }
}


/* State machine helper functions */


static inline bool process_ledcon(void)
{
    bool success = false;
    RGB_S *led_array = NULL;
    uint32_t led_array_size = 0U;

    if (LEDCON_IPC_mutex_take()) {
        LEDCON_IPC_get_led_array(&led_array, &led_array_size);
        for (uint32_t index = 0U; index < led_array_size; index++) {
            led_array[index].red = statemachine.led_buffer.red;
            led_array[index].green = statemachine.led_buffer.green;
            led_array[index].blue = statemachine.led_buffer.blue;
        }
        LEDCON_IPC_mutex_give();
        success = true;
    }
    return success;
}


static inline bool get_serial_data(uint8_t *data)
{
    bool received = false;
    if (serial_data.received) {
        *data = serial_data.data;
        received = true;
        serial_data.received = false;
    }
    return received;
}


ISR(USART_RX_vect)
{
    serial_data.data = USART0_rx();
    serial_data.received = true;
}

