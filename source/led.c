#include "led.h"

#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"

#include "gpio.h"


void LED_init(void)
{
    GPIO_config_output(DATA_DIR_REG_B, PINB5);
}


void LED_task(void)
{
    while (true) {
        GPIO_toggle(DATA_REG_B, PINB5);
        vTaskDelay(1000U);
    }
}
