#include "ledcon_ipc.h"

#include <stdbool.h>
#include <string.h>

#include "system_config.h"

#define LED_ARRAY_SIZE SYSTEM_CONFIG_LED_ARRAY_SIZE

typedef struct {
    RGB_S led_array[LED_ARRAY_SIZE];
    uint32_t led_array_size;
    bool mutex;
    bool ready;
} ledConIpc_S;

static ledConIpc_S ledcon_ipc_vars;


void LEDCON_IPC_init(void)
{
    memset(&ledcon_ipc_vars.led_array, 0U, sizeof(RGB_S)*LED_ARRAY_SIZE);
    ledcon_ipc_vars.led_array_size = LED_ARRAY_SIZE;
    ledcon_ipc_vars.mutex = false;
    ledcon_ipc_vars.ready = false;
}


/* Reader functions */


void LEDCON_IPC_get_led_array(RGB_S **led_array_ptr, uint32_t *length)
{
    *led_array_ptr = ledcon_ipc_vars.led_array;
    *length = LED_ARRAY_SIZE;
}


/* Shared functions */


bool LEDCON_IPC_mutex_take(void)
{
    bool mutex_taken = false;
    if (ledcon_ipc_vars.mutex == false) {
        ledcon_ipc_vars.mutex = true;
        mutex_taken = true;
    }
    return mutex_taken;
}


void LEDCON_IPC_mutex_give(void)
{
    ledcon_ipc_vars.mutex = false;
}

