#ifndef USART_H
#define USART_H

#include <stdbool.h>
#include <stdint.h>


void USART0_init(uint32_t baud);
void USART0_enable_rx_interrupt(void);
void USART0_enable_tx_interrupt(void);

void USART0_tx(uint8_t data);
uint8_t USART0_rx(void);

bool USART0_is_tx_empty(void);
bool USART0_is_rx_ready(void);
bool USART0_is_frame_error(void);

uint8_t USART0_clear_rx(void);


#endif  // USART_H
