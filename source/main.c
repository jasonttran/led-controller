#include "system.h"


int main(void) 
{
    SYSTEM_init();
    SYSTEM_run();
    return -1;  // System should never return
}
