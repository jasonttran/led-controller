#include "heartbeat.h"

#include <stdint.h>

#include "gpio.h"


void HEARTBEAT_init(void)
{
    GPIO_config_output(DATA_DIR_REG_B, PINB5);
}


void HEARTBEAT_task(void)
{
    GPIO_toggle(DATA_REG_B, PINB5);
}
