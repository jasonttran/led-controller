#include "wdt.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>


void WDT_init(void)
{
    wdt_reset();

    /* Reset configuration */
    WDTCSR = 0x0;

    // Beware of WD Change Enable behavior and compiler optimization

    // Using inline ASM to configure WDT;
    wdt_enable(WDTO_2S);
    wdt_reset();
}


void WDT_reset_timer(void)
{
    wdt_reset();
}


void WDT_system_reset(void)
{
    /* Reset configuration */
    WDTCSR = 0x0;

    wdt_enable(WDTO_15MS);

    while(1 == 1);  // Wait until reset occurs
}


ISR(WDT_vect)
{
    // Do nothing
}
