#ifndef LEDCON_IPC_H
#define LEDCON_IPC_H

#include <stdbool.h>
#include <stdint.h>

#include "light_ws2812.h"


void LEDCON_IPC_init(void);

/* Reader functions */
void LEDCON_IPC_get_led_array(RGB_S **led_array_ptr, uint32_t *length);

/* Shared functions */
bool LEDCON_IPC_mutex_take(void);
void LEDCON_IPC_mutex_give(void);


#endif  // LEDCON_IPC_H
