#include "gpio.h"

#include <avr/io.h>


void GPIO_config_mode(SFR_t* data_dir_reg, uint8_t pin, bool output)
{
    if (output) {
        *data_dir_reg |= (1U << pin);
    }
    else {
        *data_dir_reg &= ~(1U << pin);
    }
}


void GPIO_config_output(SFR_t* data_dir_reg, uint8_t pin)
{
    *data_dir_reg |= (1U << pin);
}


void GPIO_config_input(SFR_t* data_dir_reg, uint8_t pin)
{
    *data_dir_reg &= ~(1U << pin);
}


void GPIO_set(SFR_t* data_reg, uint8_t pin)
{
    *data_reg |= (1U << pin);
}


void GPIO_clear(SFR_t* data_reg, uint8_t pin)
{
    *data_reg &= ~(1U << pin);
}


void GPIO_toggle(SFR_t* data_reg, uint8_t pin)
{
    *data_reg ^= (1U << pin);
}


bool GPIO_get(SFR_t* input_reg, uint8_t pin)
{
    return (bool)(*input_reg & (1U << pin));
}
