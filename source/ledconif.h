/*
 * LED Controller Interface
 */

#ifndef LEDCONIF_H
#define LEDCONIF_H


void LEDCONIF_init(void);
void LEDCONIF_task(void);


#endif  // LEDCONIF_H
