#include "ledcon.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "gpio.h"
#include "ledcon_ipc.h"
#include "light_ws2812.h"


void LEDCON_init(void)
{
    GPIO_config_output(DATA_DIR_REG_B, ws2812_pin);
}


void LEDCON_task(void)
{
    RGB_S *led_array = NULL;
    uint32_t led_array_length = 0U;

    if (LEDCON_IPC_mutex_take()) {
        LEDCON_IPC_get_led_array(&led_array, &led_array_length);
        ws2812_setleds(led_array, (uint16_t)led_array_length);
        LEDCON_IPC_mutex_give();
    }
}
